.. bucky documentation master file, created by
   sphinx-quickstart on Fri Aug 14 16:30:32 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bucky's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   install
   usage
   compartment_model
   cli
   modules
   references

#.. image:: ../../logo.png

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
